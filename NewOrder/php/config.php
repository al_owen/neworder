<?php

define('DBSERVER', 'localhost'); // Database server
define('DBUSERNAME', 'root'); // Database username
define('DBPASSWORD', 'super3'); // Database password
define('DBNAME', 'test'); // Database name

/* connect to MySQL database */
$db = mysqli_connect(DBSERVER, DBUSERNAME, DBPASSWORD, DBNAME);

// Check db connection

if($db === false){
    die("Error: connection error. " . mysqli_connect_error());
}
?>

